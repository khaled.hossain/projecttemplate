package edu.missouristate.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.missouristate.domain.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	// We are using Spring Data JPA to get data using a pattern
    Optional<User> findByEmailAddress(String emailAddress);
}